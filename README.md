# init_ubuntu_essential

This script allows you to quickly boot your PC under an Ubuntu distribution.

It's allow you to :
* install essential like (git, vim, gpick, peek, meld, curl, wget, ssh, xsel, etc...)
* nvm
* custom aliases
* custom bashrc
* improve autocomplete
* improve git config
* install docker
* install portainer
* install discord
* install teams
* install jetbrains toolbox
* install vmware
* install slack
* install_typora
* install spotify
* generate ssh
