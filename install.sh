#! /bin/bash

# variables

## script

files="./.files"
dir_tmp="/tmp/install"
dir_temp_bis="."

login=$1

## fake install ?

fake=0

## colors


C_RST="0m"

ESC="\033["
T_RED=$ESC"1;31m"
T_YELLOW=$ESC"0;33m"
T_BWHITE=$ESC"1;37m"
T_RST=$ESC$C_RST

BACKGROUND="\e["
B_RED=$BACKGROUND"1;41m"
B_GREEN=$BACKGROUND"1;42m"
B_RST=$BACKGROUND$C_RST


function init_script {
	if test $UID -eq 0; then
		echo -e "$B_RED>> Ne pas lancer ce script en tant que sudo <<$B_RST"
		exit 1
	fi
	
	rm -rf $dir_tmp
	mkdir $dir_tmp
}

function finalize_script {
	rm -rf $dir_tmp
	echo -e $B_GREEN"Votre ordinateur est initialisé avec tout ce dont vous avez besoin !"$B_RST
}

function handle_error
{
	result=$1
	if test $result -eq 0; then
		return
	else
		echo -en $T_RED"[Erreur]"$T_RST" lors de l'installation, voulez-vous arreter le script ? [O/n]  "
		read stop_script
		case $stop_script in
			n|N)	return;;
			*)	exit 1;;
		esac
	fi
}

function sys_install {
	sudo apt -y install "$@";
}

function sys_upgrade
{
	echo -e "update and upgrade"

	(
		sudo apt -y update
		sudo apt -y upgrade
	) &> /dev/null
	handle_error $?

}

function install_essential {
	echo -e "Install git vim gpick peek meld shutter curl wget ssh xsel pwgen acpi jq gcc build-essential"
	sys_install git vim gpick peek meld shutter curl wget ssh xsel pwgen acpi jq gcc build-essential &> /dev/null
	handle_error $?
}

function install_nvm {
	read -p "Installer NVM ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
	    		source ~/.bashrc
	    		# nvm install --lts
	    	) &> /dev/null
	    ;;
	esac
	handle_error $?
}

function custom_aliases {
	read -p "use custom aliases ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		echo "alias open='f () { nautilus \"\$1\" </dev/null &>/dev/null &}; f'" | tee -a ~/.bash_aliases
			source ~/.bashrc
	    	) &> /dev/null
	    ;;
	esac
	handle_error $?
}

function custom_bashrc {
	read -p "use custom bashrc ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		echo "
if [ -f ~/.bash_custom ]; then 
	. ~/.bash_custom
fi" | tee -a ~/.bashrc
	    		
	    		echo "PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;90m\]\t \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00;33m\]\$(__git_ps1)\[\033[00m\]\\$ '
export HISTTIMEFORMAT=\"%Y/%m/%d - %H:%M:%S \"
export HISTFILESIZE=10000000
export HISTIGNORE=ls

TIMEFORMAT='%3lR'" | tee -a ~/.bash_custom
	    		
			source ~/.bashrc
	    	) &> /dev/null
	    ;;
	esac
	handle_error $?
}

function improve_autocomplete {
	read -p "improve autocomplete ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		echo "\$include /etc/inputrc
set completion-ignore-case On
set show-all-if-ambiguous On" | tee -a ~/.inputrc
			source ~/.bashrc
	    	) &> /dev/null
	    ;;
	esac
	handle_error $?
}

function improve_git_config {
	read -p "improve git config ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    		while [ -z $git_config_email ]
			do
				read -p "email : " git_config_email
			done
	    		
	    		while [ -z $git_config_last_name ]
			do
				read -p "nom : " git_config_last_name
			done
	    		
	    		while [ -z $git_config_first_name ]
	    		do
				read -p "prenom : " git_config_first_name
			done
			
			(
				echo "[color]
    diff = auto
    status = auto
    branch = auto
[alias]
    ci = commit
    co = checkout
    st = status
    br = branch
    l = log --pretty=format:'%C(yellow)%h %Cred%ad %Cblue%an%Cgreen%d %Creset%s' --date=iso8601
    lv = show --pretty=oneline --name-status
    la = log --pretty=format:'%h -> %H' -n 1
    addww=git diff -U0 -w --no-color | git apply --cached --ignore-whitespace --unidiff-zero -
    addww=!sh -c 'git diff -U0 -w --no-color \"\$@\" | git apply --cached --ignore-whitespace --unidiff-zero -'
    graph1 = log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all
    graph2 = log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
    graph = !\"git graph1\"
[core]
    editor = vi
    fileMode = false
    commentChar = \";\"
[push]
    default = simple
[user]
    email = $git_config_email
    name = $git_config_first_name $git_config_last_name
[pager]
    status = false
[merge]
    tool = meld
[fetch]
        pruneTags = true
        prune = true" | tee -a ~/.gitconfig
			) &> /dev/null
	    ;;
	esac
	handle_error $?
}

function improve_git_ignore {
		read -p "add global .gitconfig ? [O/n]" yn
  	case $yn in
  	    [Nn]* );;
  	    * )
  	    	(
  	    		echo ".idea" | tee -a ~/.gitignore
        git config --global core.excludesFile '~/.gitignore'
  			source ~/.bashrc
  	    	) &> /dev/null
  	    ;;
  	esac
  	handle_error $?
}

function install_docker {
	read -p "install docker ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		sys_install docker-compose
	    		sudo usermod -aG docker $USER
			source ~/.bashrc
	    	) &> /dev/null
	    	
	    	install_portainer
	    ;;
	esac
	handle_error $?
}

function install_portainer {
	read -p "install portainer ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		sudo docker volume create portainer_data
	    		sudo docker run -d -p :8000 -p :9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
	    		sudo docker inspect portainer | sudo jq -r '.[] | {ip: .NetworkSettings.IPAddress} | .ip+" portainer.local"' | sudo tee -a /etc/hosts
	    	) &> /dev/null
	    ;;
	esac
	
	echo "http://portainer.local:9000"
	
	handle_error $?
}

function install_discord {
	read -p "install discord ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
          sudo snap install discord
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_teams {
	read -p "install teams ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
          sudo snap install teams
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_jetbrains_toolbox {
	read -p "install jetbrains toolbox ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		wget -q "https://download.jetbrains.com/toolbox/jetbrains-toolbox-2.3.0.30876.tar.gz"
	    		tar xzf $dir_temp_bis/jetbrains-toolbox-2.3.0.30876.tar.gz
	    		rm -rf $dir_temp_bis/jetbrains-toolbox-2.3.0.30876.tar.gz
	    		chmod +x $dir_temp_bis/jetbrains-toolbox-2.3.0.30876/jetbrains-toolbox
	    		$dir_temp_bis/jetbrains-toolbox-2.3.0.30876/jetbrains-toolbox
	    		rm -rf $dir_temp_bis/jetbrains-toolbox-2.3.0.30876
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_vmware {
	read -p "install vmware ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
	    		wget -q -O ./vmware "https://download3.vmware.com/software/wkst/file/VMware-Workstation-Full-16.1.1-17801498.x86_64.bundle"
	    		chmod +x ./vmware
	    		sudo ./vmware
	    		rm -rf ./vmware
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_slack {
	read -p "install slack ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
          sudo snap install slack --classic
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_typora {
	read -p "install typora ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
          sudo snap install typora
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_spotify {
	read -p "install spotify ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	(
          sudo snap install spotify
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function generate_ssh {
	read -p "generate ssh ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * ) 
	    	
      while [ -z $email ]
			do
				read -p "email : " email
			done
			
			while [ -z $password ]
			do
				echo -n "password : "; read -s password; echo ""
			done
			
	    	(	
	    		ssh-keygen -a 100 -f /home/${USER}/.ssh/id_rsa -N $password -o -t ed25519 -C "$email"
	    	) &> /dev/null
	    ;;
	esac
	
	handle_error $?
}

function install_mailspring {
	read -p "install mailspring ? [O/n]" yn
	case $yn in
	    [Nn]* );;
	    * )

	    	(
	    		sudo snap install mailspring
	    	) &> /dev/null
	    ;;
	esac

	handle_error $?
}


init_script

sys_upgrade
install_essential
install_nvm
custom_aliases
custom_bashrc
improve_autocomplete
improve_git_config
improve_git_ignore
install_docker
install_discord
install_teams
install_jetbrains_toolbox
install_vmware
install_slack
install_typora
install_spotify
generate_ssh
install_mailspring

finalize_script
